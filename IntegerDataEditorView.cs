﻿using UnityEngine;
using UnityMVC;
using DataEditorUIs;

public class IntegerDataEditorView : View
{
    public static IntegerDataEditorView Attach(GameObject parent)
    {
        var root = new GameObject("IntegerDataEditor");
        var view = root.AddComponent<IntegerDataEditorView>();
        view.SetParent(parent);
        return view;
    }

    private IntegerDataEditor _editor = null;
    private UnityEngine.UI.Text _text = null;
    private UnityEngine.UI.Image _frame = null;
    private static Color _defaultFrameColor = new Color(0.15f, 0.15f, 0.15f, 0.3f);

    private float _width = 0;
    private float _height = 0;

    public IntegerDataEditorView SetUp(
        IntegerDataEditor editor,
        float width,
        float height,
        Position position = null)
    {
        SetModel(editor);
        SetSize(_width = width, _height = height);

        if (position != null)
        {
            editor.position = position;
        }
        UpdatePosition();

        return this;
    }

    public IntegerDataEditorView SetModel(IntegerDataEditor editor)
    {
        base.SetModel<IntegerDataEditor>(_editor = editor);

        editor.OnValueUpdated += () => {
            _text.text = editor.raw.ToString();
        };

        return this;
    }

    public IntegerDataEditorView SetSize(
        float width,
        float height,
        int fontSize = -1)
    {
        SetUpFrame(width, height);

        var textRoot = new GameObject("text");
        textRoot.transform.parent = GetRoot().transform;
        textRoot.transform.localPosition = Vector3.zero;
        textRoot.transform.localRotation = Quaternion.identity;
        textRoot.transform.localScale = Vector3.one;
        var text = _text = textRoot.AddComponent<UnityEngine.UI.Text>();
        var textFontsize = fontSize > 0 ? fontSize : Mathf.CeilToInt(height * 0.8f);
        text.text = _editor.raw.ToString();
        text.font = DataEditorUIs.FontManager.Create();
        text.fontStyle = FontStyle.Bold;
        text.alignment = TextAnchor.MiddleCenter;
        text.fontSize = textFontsize;
        text.color = Color.white;

        var rectTransform = textRoot.GetComponent<UnityEngine.RectTransform>();
        if (rectTransform == null)
        {
            rectTransform = textRoot.AddComponent<UnityEngine.RectTransform>();
        }
        rectTransform.sizeDelta = new Vector2(width, height);

        return this;
    }


    public IntegerDataEditorView SetTextColor(Color color)
    {
        if (_text == null) return this;
        _text.color = color;
        return this;
    }

    public IntegerDataEditorView SetTextAnchor(TextAnchor anchor)
    {
        if (_text == null) return this;
        _text.alignment = anchor;
        return this;
    }

    public IntegerDataEditorView SetFrameColor(Color color)
    {
        if (_frame == null) return this;
        _frame.color = color;
        return this;
    }

    public IntegerDataEditorView SetInputField()
    {
        var rectTransform = GetRoot().GetComponent<UnityEngine.RectTransform>();
        if (rectTransform == null)
        {
            rectTransform = GetRoot().AddComponent<UnityEngine.RectTransform>();
        }

        if (_text == null)
        {
            SetSize(rectTransform.sizeDelta.x, rectTransform.sizeDelta.y);
        }
        
        var input = GetRoot().AddComponent<UnityEngine.UI.InputField>();
        input.textComponent = _text;
        input.onEndEdit.RemoveListener(text => {
            _editor.SetValue(int.Parse(text));
        });

        return this;
    }

    public IntegerDataEditorView SetSlider(
        int min,
        int max)
    {
        var bar = NumberVariableBarView.Attach(GetRoot())
            .SetUp(
            _editor,
            min,
            max,
            200,
            30);
        bar.GetRoot().transform.localPosition = new Vector3(0, -50, 0);


        return this;
    }

    public IntegerDataEditorView SetDigitPanels(int digit)
    {
        var size = Mathf.FloorToInt(_text.fontSize * 1.1f);
        SetTextAnchor(TextAnchor.MiddleRight);
        var panels = NumberDigitPanelsView.Attach(GetRoot()).SetUp(
            _editor,
            digit,
            size);
        panels.GetRoot().transform.localPosition = new Vector3(
            _width * 0.5f - size * 0.25f,
            0,0
            );

        return this;
    }

    public IntegerDataEditorView SetTitle(
        string title,
        Position offset,
        Color color
        )
    {
        var textRoot = new GameObject("title");
        textRoot.transform.parent = GetRoot().transform;
        textRoot.transform.localPosition = offset.ToVector3();
        textRoot.transform.localRotation = Quaternion.identity;
        textRoot.transform.localScale = Vector3.one;
        var text = textRoot.AddComponent<UnityEngine.UI.Text>();
        var textFontsize = Mathf.CeilToInt(_height * 0.8f);
        text.text = _editor.raw.ToString();
        text.font = FontManager.Create();
        text.fontStyle = FontStyle.Bold;
        text.alignment = TextAnchor.MiddleLeft;
        text.fontSize = textFontsize;
        text.color = color;

        var rectTransform = textRoot.GetComponent<UnityEngine.RectTransform>();
        if (rectTransform == null)
        {
            rectTransform = textRoot.AddComponent<UnityEngine.RectTransform>();
        }
        rectTransform.sizeDelta = new Vector2(_width, _height);

        text.text = title;

        return this;
    }


    private void SetUpFrame(float width, float height)
    {
        var frameRoot = new GameObject("frame");
        frameRoot.transform.parent = GetRoot().transform;
        frameRoot.transform.localPosition = Vector3.zero;
        frameRoot.transform.localRotation = Quaternion.identity;
        frameRoot.transform.localScale = Vector3.one;
        var frameImage = _frame = frameRoot.AddComponent<UnityEngine.UI.Image>();
        frameImage.sprite = Sprite.Create(null, new Rect(0, 0, width, height), Vector2.one / 2);
        frameImage.color = _defaultFrameColor;

        var rectTransform = frameRoot.GetComponent<UnityEngine.RectTransform>();
        if (rectTransform == null)
        {
            rectTransform = frameRoot.AddComponent<UnityEngine.RectTransform>();
        }
        rectTransform.sizeDelta = new Vector2(width, height);
    }
}

