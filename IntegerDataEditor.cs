﻿public class IntegerDataEditor : ElementDataEditor
{
    public int raw;
    public int min = int.MinValue;
    public int max = int.MaxValue;

    public void SetValue(int newValue)
    {
        if (newValue == raw) return;
        if (newValue < min) newValue = min;
        if (newValue > max) newValue = max;

        raw = newValue;
        UpdateValue();
    }
}

