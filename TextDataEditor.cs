﻿public class TextDataEditor : ElementDataEditor
{
    public string raw;

    public void SetValue(string newValue)
    {
        if (newValue == raw) return;

        raw = newValue;
        UpdateValue();
    }
}

