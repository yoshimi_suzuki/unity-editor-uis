﻿using UnityMVC;

public class ChoiceDataElement : Model
{
    public string raw;
    public float width = 0;
    public float height = 0;
    public bool forceText = false;

    public event EventHandler OnSeleted;

    public void Select()
    {
        if (OnSeleted != null) OnSeleted();
    }

    public bool isIcon
    {
        get
        {
            if (forceText) return false;
            return raw.Contains(".png") || raw.Contains(".jpg");
        }
    }
}

