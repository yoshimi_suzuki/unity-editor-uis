﻿using UnityEngine;
using UnityMVC;

public class NumberDigitPanelsView : View
{
    public static NumberDigitPanelsView Attach(GameObject parent)
    {
        var root = new GameObject("NumberDigitPanels");
        var view = root.AddComponent<NumberDigitPanelsView>();
        view.SetParent(parent);
        return view;
    }

    private IntegerDataEditor _editor = null;
    private int _maxDigit = 1;
    private int _size = 48;

    public NumberDigitPanelsView SetUp(
        IntegerDataEditor editor,
        int maxDigit,
        int size)
    {
        _editor = editor;
        _maxDigit = maxDigit;
        _size = size;
        
        return this;
    }

    private void Start()
    {
        for(var i = 1; i <= _maxDigit; i++)
        {
            AddDigitPanel(i);
        }
    }



    private void AddDigitPanel(int digit)
    {
        if (digit <= 0) return;

        var offset = new Vector3(
            -1 * _size * 0.5f * (digit - 1),
            _size * 0.75f,
            0);

        TextButtonView.Attach(GetRoot())
            .SetText("+", Mathf.FloorToInt( _size * 0.40f))
            .SetSize(_size * 0.5f, _size * 0.5f)
            .SetPosition(new Position(offset.x, offset.y))
            .SetBold()
            .RegisterCallback(() =>
            {
                _editor.SetValue(_editor.raw + Mathf.FloorToInt(Mathf.Pow(10, (digit - 1))));
            });

        TextButtonView.Attach(GetRoot())
            .SetText("-", Mathf.FloorToInt(_size * 0.40f))
            .SetSize(_size * 0.5f, _size * 0.5f)
            .SetPosition(new Position(offset.x, -1 * offset.y))
            .SetBold()
            .RegisterCallback(() =>
            {
                _editor.SetValue(_editor.raw - Mathf.FloorToInt(Mathf.Pow(10, (digit - 1))));
            });
    }
}

