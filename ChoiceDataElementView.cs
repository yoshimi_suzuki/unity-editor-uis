﻿using System.Collections.Generic;

using UnityEngine;
using UnityMVC;

using DataEditorUIs;

public class ChoiceDataElementView : View
{
    public static ChoiceDataElementView Attach(GameObject parent)
    {
        var root = new GameObject("ChoiceDataElement");
        var view = root.AddComponent<ChoiceDataElementView>();
        view.SetParent(parent);
        return view;
    }

    private GameObject _check = null;

    private ChoiceDataElement _element;
    public ChoiceDataElementView SetUp(
        ChoiceDataElement element,
        int fontSize)
    {
        base.SetModel<ChoiceDataElement>(_element = element);

        var width = element.width > 0 ? element.width : 200;
        var height = element.height > 0 ? element.height : 50;

        AddFrame(width, height);

        float checkWidth = 20;
        float checkHeight = 20;

        if (element.isIcon)
        {
            AddIcon(element, width, height,
            ()=> {
                _check = AddCheckMark(width, height, checkWidth, checkHeight);

                AddCollision(width, height)
                .OnClicked += () =>
                {
                    element.Select();
                };
            });
        }
        else
        {
            AddTitle(element, width, height, fontSize);
            _check = AddCheckMark(width, height, checkWidth, checkHeight);
            AddCollision(width, height)
            .OnClicked += () =>
            {
                element.Select();
            };
        }

        
        UpdatePosition();

        return this;
    }

    public void Check()
    {
        if (_check == null) return;
        _check.SetActive(true);
    }

    public void UnCheck()
    {
        if (_check == null) return;
        _check.SetActive(false);
    }


    private void AddFrame(float width, float height)
    {
        var frameRoot = new GameObject("frame");
        frameRoot.transform.parent = GetRoot().transform;
        frameRoot.transform.localPosition = Vector3.zero;
        frameRoot.transform.localRotation = Quaternion.identity;
        frameRoot.transform.localScale = Vector3.one;
        var frameRectTransform = frameRoot.AddComponent<RectTransform>();
        frameRectTransform.sizeDelta = new Vector2(width, height);
        var frameImage = frameRoot.AddComponent<UnityEngine.UI.Image>();
        frameImage.sprite = null;
        frameImage.color = new Color(0.15f, 0.15f, 0.15f, 0.3f);
    }

    private GameObject AddCheckMark(
        float width, float height,
        float checkWidth, float checkHeight)
    {
        var checkRoot = new GameObject("check");
        checkRoot.transform.parent = GetRoot().transform;
        checkRoot.transform.localPosition = new Vector3(width * -0.5f + checkWidth / 2, height * 0.5f - checkHeight / 2);
        checkRoot.transform.localRotation = Quaternion.identity;
        checkRoot.transform.localScale = Vector3.one;
        var checkRectTransform = checkRoot.AddComponent<RectTransform>();
        checkRectTransform.sizeDelta = new Vector2(checkWidth, checkHeight);
        var checkImage = checkRoot.AddComponent<UnityEngine.UI.Image>();
        checkImage.sprite = null;
        checkImage.color = new Color(1f, 0.15f, 0.15f, 1f);
        checkRoot.SetActive(false);

        return checkRoot;
    }


    private GameObject _collisionRoot = null;
    public void Deactivate()
    {
        if (_collisionRoot == null) return;
        _collisionRoot.SetActive(false);
    }
    public void Activate()
    {
        if (_collisionRoot == null) return;
        _collisionRoot.SetActive(true);
    }

    private Controller AddCollision(float width, float height)
    {
        var collisionRoot = _collisionRoot = new GameObject("collision");
        collisionRoot.transform.parent = GetRoot().transform;
        collisionRoot.transform.localPosition = Vector3.zero;
        collisionRoot.transform.localRotation = Quaternion.identity;
        collisionRoot.transform.localScale = Vector3.one;
        var controller = collisionRoot.AddComponent<Controller>();
        var collider = collisionRoot.AddComponent<BoxCollider2D>();
        collider.size = new Vector2(width, height);

        return controller;
    }

    private void AddTitle(
        ChoiceDataElement element,
        float width,
        float height,
        int fontSize)
    {
        var textRoot = new GameObject("text");
        textRoot.transform.parent = GetRoot().transform;
        textRoot.transform.localPosition = Vector3.zero;
        textRoot.transform.localRotation = Quaternion.identity;
        textRoot.transform.localScale = Vector3.one;
        var textRectTransform = textRoot.AddComponent<RectTransform>();
        textRectTransform.sizeDelta = new Vector2(width, height);
        var textUI = textRoot.AddComponent<UnityEngine.UI.Text>();
        textUI.font = FontManager.Create();
        textUI.fontSize = fontSize;
        textUI.alignment = TextAnchor.MiddleCenter;
        textUI.color = new Color(1, 1, 1, 1);
        textUI.text = element.raw;
    }

    private void AddIcon(
        ChoiceDataElement element,
        float width,
        float height,
        System.Action callback)
    {
        new ImageComposer(element.raw)
            .SetParent(GetRoot().transform)
            .SetSize(width, height)
            .Show(rectTransform => {
                if (isDetached)
                {
                    GameObject.Destroy(rectTransform.gameObject);
                    return;
                }

                callback();
            });
    }
}

