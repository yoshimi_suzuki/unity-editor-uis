﻿using UnityMVC;

public class ElementDataEditor : Model
{
    public event EventHandler OnValueUpdated;
    public event EventHandler OnFinished;

    public void Finish()
    {
        if (OnFinished != null) OnFinished();
    }

    protected void UpdateValue()
    {
        if (OnValueUpdated != null) OnValueUpdated();
    }
}