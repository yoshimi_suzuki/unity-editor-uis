﻿using UnityEngine;
using UnityMVC;
using DataEditorUIs;

public class ListElementView : View
{
    public static ListElementView Attach(GameObject parent)
    {
        var root = new GameObject("list element");
        root.transform.parent = parent.transform;
        root.transform.localPosition = Vector3.zero;
        root.transform.localRotation = Quaternion.identity;
        root.transform.localScale = Vector3.one;

        var view = root.AddComponent<ListElementView>();

        return view;
    }

    private ListElement _element;
    virtual public ListElementView SetUp(ListElement model)
    {
        base.SetModel<ListElement>(_element = model);

        UpdatePosition();

        return this;
    }

    public ListElementView ShowAsElement(Vector2 size, int fontSize)
    {
        var model = _element;
        
        gameObject.AddComponent<RectTransform>().sizeDelta = size;
        var title = gameObject.AddComponent<UnityEngine.UI.Text>();
        title.font = FontManager.Create();
        title.fontSize = fontSize;
        title.alignment = TextAnchor.MiddleCenter;
        title.text = model.title;

        if (model.frameClickEnabledAsElement)
        {
            var collider = gameObject.AddComponent<BoxCollider2D>();
            collider.size = size;
            var controller = gameObject.AddComponent<Controller>();
            controller.OnClicked += () => {
                model.Select();
            };
        }

        return this;
    }
}