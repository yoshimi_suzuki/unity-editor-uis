﻿using UnityMVC;

public class ListElement : Model
{
    public event EventHandler OnSelected;

    virtual public void Select()
    {
        if (OnSelected != null) OnSelected();
    }

    public string title
    {
        get;
        set;
    }

    public bool frameClickEnabledAsElement
    {
        get;
        set;
    } = true;
}

