﻿using System.Collections.Generic;

using UnityEngine;
using UnityMVC;

public class ListContainerView : View
{
    public static ListContainerView Attach(GameObject parent, Vector2 size)
    {
        var root = new GameObject("ListContainer");
        var view = root.AddComponent<ListContainerView>();

        var scrollRectRoot = new GameObject("scroller");
        scrollRectRoot.transform.parent = root.transform;
        scrollRectRoot.transform.localPosition = new Vector3(0, 0, 0);
        scrollRectRoot.transform.localRotation = Quaternion.identity;
        scrollRectRoot.transform.localScale = Vector3.one;
        var scrollRect = scrollRectRoot.AddComponent<ScrollRectView>();
        var controller = scrollRectRoot.AddComponent<Controller>();
        var collider = scrollRectRoot.AddComponent<BoxCollider2D>();
        collider.size = size;
        scrollRect.AssignController(controller);

        var container = new GameObject("container");
        container.transform.parent = root.transform;
        container.transform.localPosition = new Vector3(0, 0, -1);
        container.transform.localRotation = Quaternion.identity;
        container.transform.localScale = Vector3.one;
        container.AddComponent<RectTransform>().sizeDelta = size;
        var mask = container.AddComponent<UnityEngine.UI.RectMask2D>();

        view.SetAttachments(scrollRect, container);

        view.SetParent(parent);
        return view;
    }

    private Scroller _scroller;
    private ScrollRectView _scrollRect;
    private GameObject _container;

    public override void Detach()
    {
        foreach (var view in _elementViews) view.Detach();
        base.Detach();
    }

    public ListContainerView SetAttachments(
        ScrollRectView scrollRect,
        GameObject container)
    {
        _scrollRect = scrollRect;
        _container = container;
                return this;
    }

    private List<ListElementView> _elementViews = null;
    public ListContainerView SetUp(
        List<ListElement> molds,
        Position offset,
        System.Func<GameObject, ListElementView> attach)
    {
        var scroller = _scroller = new Scroller();

        var bottomY = 0f;

        _elementViews = new List<ListElementView>();

        foreach (var mold in molds)
        {
            var view = attach(_container).SetUp(mold);

            if (bottomY > mold.position.y) bottomY = mold.position.y;

            scroller.AddModel(mold);
            _elementViews.Add(view);
        }

        scroller.SetLimit(0, -1 * bottomY, 0, 0);
        scroller.SetOffset(offset.x, offset.y);
        scroller.SetFriction(1,1);

        _scrollRect.SetScroller(scroller);

        return this;
    }

    public void ShowAsElement(Vector2 size, int fontSize)
    {
        foreach(var view in _elementViews)
        {
            view.ShowAsElement(size, fontSize);
        }
    }


    public Position currentOffset
    {
        get
        {
            return _scroller != null ? _scroller.currentOffset : new Position();
        }
    }
}