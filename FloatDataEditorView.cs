﻿using UnityEngine;
using UnityMVC;

using DataEditorUIs;

public class FloatDataEditorView : View
{
    public static FloatDataEditorView Attach(GameObject parent)
    {
        var root = new GameObject("FloatDataEditor");
        var view = root.AddComponent<FloatDataEditorView>();
        view.SetParent(parent);
        return view;
    }

    private FloatDataEditor _editor = null;
    private UnityEngine.UI.Text _text = null;
    private UnityEngine.UI.Image _frame = null;
    private static Color _textColor = new Color(1, 1, 1, 1);
    private static Color _defaultTextColor = new Color(0.4f, 0.4f, 0.4f, 1);
    private static Color _defaultFrameColor = new Color(0.15f, 0.15f, 0.15f, 0.3f);

    public FloatDataEditorView SetUp(
        FloatDataEditor editor,
        float width,
        float height,
        Position position = null)
    {
        SetModel(editor);
        SetSize(width, height);

        if (position != null)
        {
            editor.position = position;
        }
        UpdatePosition();

        return this;
    }

    public FloatDataEditorView SetModel(FloatDataEditor editor)
    {
        base.SetModel<FloatDataEditor>(_editor = editor);

        editor.raw.ToString();

        return this;
    }

    public FloatDataEditorView SetSize(
        float width,
        float height,
        int fontSize = -1)
    {
        SetUpFrame(width, height);

        var textRoot = new GameObject("text");
        textRoot.transform.parent = GetRoot().transform;
        textRoot.transform.localPosition = Vector3.zero;
        textRoot.transform.localRotation = Quaternion.identity;
        textRoot.transform.localScale = Vector3.one;
        var text = _text = textRoot.AddComponent<UnityEngine.UI.Text>();
        var textFontsize = fontSize > 0 ? fontSize : Mathf.CeilToInt(height * 0.8f);
        text.text = _editor.raw.ToString();
        text.font = FontManager.Create();
        text.fontStyle = FontStyle.Bold;
        text.alignment = TextAnchor.MiddleCenter;
        text.fontSize = textFontsize;
        text.color = Color.white;

        var rectTransform = textRoot.GetComponent<UnityEngine.RectTransform>();
        if (rectTransform == null)
        {
            rectTransform = textRoot.AddComponent<UnityEngine.RectTransform>();
        }
        rectTransform.sizeDelta = new Vector2(width, height);

        return this;
    }


    public FloatDataEditorView SetTextColor(Color color)
    {
        if (_text == null) return this;
        _text.color = _textColor = color;
        return this;
    }

    public FloatDataEditorView SetDefaultTextColor(Color color)
    {
        if (_text == null) return this;
        _defaultTextColor = color;
        return this;
    }

    public FloatDataEditorView SetTextAnchor(TextAnchor anchor)
    {
        if (_text == null) return this;
        _text.alignment = anchor;
        return this;
    }

    public FloatDataEditorView SetFrameColor(Color color)
    {
        if (_frame == null) return this;
        _frame.color = color;
        return this;
    }

    public FloatDataEditorView SetInputField()
    {
        var rectTransform = GetRoot().GetComponent<UnityEngine.RectTransform>();
        if (rectTransform == null)
        {
            rectTransform = GetRoot().AddComponent<UnityEngine.RectTransform>();
        }

        if (_text == null)
        {
            SetSize(rectTransform.sizeDelta.x, rectTransform.sizeDelta.y);
        }
        
        var input = GetRoot().AddComponent<UnityEngine.UI.InputField>();
        input.textComponent = _text;
        input.onEndEdit.AddListener(text => {
            if (text == "")
            {
                input.text =
                _text.text = "0";
                _editor.SetValue(0);
            }
            else
            {
                _editor.SetValue(float.Parse(text));

            }
        });

        input.text = _editor.raw.ToString();

        return this;
    }



    private void SetUpFrame(float width, float height)
    {
        var frameRoot = new GameObject("frame");
        frameRoot.transform.parent = GetRoot().transform;
        frameRoot.transform.localPosition = Vector3.zero;
        frameRoot.transform.localRotation = Quaternion.identity;
        frameRoot.transform.localScale = Vector3.one;
        var frameImage = _frame = frameRoot.AddComponent<UnityEngine.UI.Image>();
        frameImage.sprite = Sprite.Create(null, new Rect(0, 0, width, height), Vector2.one / 2);
        frameImage.color = _defaultFrameColor;

        var rectTransform = frameRoot.GetComponent<UnityEngine.RectTransform>();
        if (rectTransform == null)
        {
            rectTransform = frameRoot.AddComponent<UnityEngine.RectTransform>();
        }
        rectTransform.sizeDelta = new Vector2(width, height);
    }
}

