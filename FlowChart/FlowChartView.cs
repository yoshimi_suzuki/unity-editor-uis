﻿using System.Collections.Generic;
using UnityEngine;
using UnityMVC;

public class FlowChartView : View
{
    public static FlowChartView Attach(GameObject parent)
    {
        var view = View.Attach<FlowChartView>(parent);
        return view;
    }

    private FlowChart _chart;
    private System.Func<GameObject, FlowChartNodeView> _nodeViewCreator;

    private GameObject _nodesRoot;
    private GameObject _linksRoot;

    private Dictionary<int, View> _nodeViews = new Dictionary<int, View>();
    private List<FlowChartLinkView> _linkViews = new List<FlowChartLinkView>();

    private GameObject _maskNode;
    private GameObject _scrollerNode;

    private System.Func<GameObject, FlowChartLinkView> _linkCreator = null;

    public FlowChartView SetUp(
        FlowChart chart,
        System.Func<GameObject, FlowChartNodeView> nodeViewCreator,
        System.Func<GameObject, FlowChartLinkView> linkCreator = null
        )
    {
        _chart = chart;
        _nodeViewCreator = nodeViewCreator;
        _linkCreator = linkCreator != null
            ? linkCreator
            : parent => {
                return FlowChartLinkView.Attach<FlowChartLinkView>(parent);
            };

        SetUpFrame();

        _linksRoot = ViewUtils.AddChild(_scrollerNode, "links");
        _linksRoot.transform.localPosition = Vector3.forward * -10;

        _nodesRoot = ViewUtils.AddChild(_scrollerNode, "nodes");
        _nodesRoot.transform.localPosition = Vector3.forward * -11;

        UpdateNodes();
        UpdateLinks();

        _scrollerNode.transform.localPosition = chart.position.ToVector3();

        foreach (var node in _chart.nodes)
        {
            node.OnPositionUpdated += () => {
                foreach (var nextId in node.nextNodes)
                {
                    var linkView = _linkViews.Find(target => 
                    {
                        return target.link.from == node.id
                            && target.link.to == nextId;
                    });
                    if (linkView == null) continue;
                    linkView.UpdateShape(
                        _nodeViews[node.id].transform.localPosition,
                        _nodeViews[nextId].transform.localPosition,
                        _chart.lineWidth);
                }
                foreach (var prevId in node.prevNodes)
                {
                    var linkView = _linkViews.Find(target => 
                    {
                        return target.link.from == prevId
                            && target.link.to == node.id;
                    });
                    if (linkView == null) continue;
                    linkView.UpdateShape(
                        _nodeViews[prevId].transform.localPosition,
                        _nodeViews[node.id].transform.localPosition,
                        _chart.lineWidth);
                }
            };
        }

        _chart.OnListUpdated += () => {
            UpdateNodes();
        };
        _chart.OnLinkUpdated += () => {
            UpdateLinks();
        };

        return this;
    }

    private void SetUpFrame()
    {
        _maskNode = ViewUtils.AddChild(GetRoot(), "mask");
        var scroller = _scrollerNode = ViewUtils.AddChild(_maskNode, "scroller");

        _maskNode.AddComponent<RectTransform>().sizeDelta = new Vector2(_chart.frameWidth, _chart.frameHeight);
        var maskImage = _maskNode.AddComponent<UnityEngine.UI.Image>();
        maskImage.sprite = Sprite.Create(Texture2D.whiteTexture, new Rect(0, 0, 1, 1), Vector2.one / 2);
        var mask = _maskNode.AddComponent<UnityEngine.UI.Mask>();
        mask.showMaskGraphic = false;

        _controller = ViewUtils.AddFrame(
            scroller,
            new Vector2(_chart.width, _chart.height),
            _chart.frameColor);

        _controller.OnTouchMoved += (diff, duration) => {
            scroller.transform.localPosition += diff;
        };
        _controller.OnTouchFinished += (position, duration, distance) => {
            if (duration < 1.0f && distance < 10.0f)
            {
                _chart.Click();
            }
        };

        _controller.OnSecondaryTouchBegan += position => {

        };
        _controller.OnSecondaryTouchMoved += (diff, duration) => {

        };
        _controller.OnSecondaryTouchFinished += (position, duration, distance) => {
            if (duration < 1.0f && distance < 10.0f)
            {
                _chart.SecondaryClick();
            }
        };
    }



    private void UpdateNodes()
    {
        foreach (var view in _nodeViews.Values) view.Detach();
        _nodeViews.Clear();

        foreach (var node in _chart.nodes)
        {
            _nodeViews.Add(
                node.id,
                 _nodeViewCreator(_nodesRoot).SetUp(node)
                );
        }
    }

    private void UpdateLinks()
    {
        foreach (var view in _linkViews) view.Detach();
        _linkViews.Clear();

        foreach (var link in _chart.links)
        {
            var view = _linkCreator(_linksRoot)
                    .SetUp(
                        link,
                        () =>
                        {
                            _chart.ClickLink(link);
                        },
                        () =>
                        {
                            _chart.SecondaryClickLink(link);
                        });

            view.UpdateShape(
                _nodeViews[link.from].transform.localPosition,
                _nodeViews[link.to].transform.localPosition,
                _chart.lineWidth);

            _linkViews.Add(view);
        }
    }

    public Position currentOffset
    {
        get
        {
            return new Position(
                _scrollerNode.transform.localPosition.x,
                _scrollerNode.transform.localPosition.y
                );
        }
    }
}

public class FlowChartNodeView : View
{
    public static FlowChartNodeView Attach(GameObject parent)
    {
        return Attach<FlowChartNodeView>(parent);
    }

    virtual public FlowChartNodeView SetUp(FlowChartNode node)
    {
        var frameRoot = ViewUtils.AddChild(GetRoot(), "frame");
        frameRoot.transform.localPosition = Vector3.forward;
        frameRoot.transform.SetAsFirstSibling();

        _controller = ViewUtils.AddFrame(
            frameRoot,
            new Vector2(node.width, node.height),
            node.frameColor);

        _controller.OnTouchMoved += (diff, duration) => {
            node.position += new Position(diff.x, diff.y);
        };
        _controller.OnTouchFinished += (position, duration, distance) => {
            if (duration < 1.0f && distance < 10.0f)
            {
                node.Click();
            }
        };


        _controller.OnSecondaryTouchMoved += (diff, duration) => {
            node.position += new Position(diff.x, diff.y);
        };
        _controller.OnSecondaryTouchFinished += (position, duration, distance) => {
            if (duration < 1.0f && distance < 10.0f)
            {
                node.SecondaryClick();
            }
        };

        return this;
    }
}

public class FlowChartLinkView : View
{
    public static FlowChartLinkView Attach(GameObject parent)
    {
        return Attach<FlowChartLinkView>(parent);
    }

    private FlowChartLink _link;

    virtual public FlowChartLinkView SetUp(
        FlowChartLink link,
        System.Action onClicked,
        System.Action onSecondaryClicked)
    {
        _link = link;
        _controller = ViewUtils.AddFrame(
            GetRoot(),
            new Vector2(1, 1),
            new Color(1, 1, 1, 1));

        _controller.OnTouchFinished += (position, duration, distance) =>
        {
            if (duration < 1.0f && distance < 10.0f)
            {
                onClicked();
            }
        };
        _controller.OnSecondaryTouchFinished += (position, duration, distance) => {
            if (duration < 1.0f && distance < 10.0f)
            {
                onSecondaryClicked();
            }
        };

        return this;
    }

    public void UpdateShape(
        Vector3 from,
        Vector3 to,
        float lineWidth
    ) {
        var x = (from.x + to.x) / 2;
        var y = (from.y + to.y) / 2;

        var radian = (to - from).x == 0 ? Mathf.PI / 2 : Mathf.Atan2((to - from).y, (to - from).x);
        var theta = 180 * radian / Mathf.PI - 90;
        var width = lineWidth;
        var height = (to - from).magnitude;

        transform.localPosition = new Vector3(x, y, 0);
        transform.localRotation = Quaternion.Euler(0,0,theta);
        _controller.gameObject.GetComponent<BoxCollider2D>().size = new Vector2(width, height);
        _controller.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(width, height);


        if (!addArrow)
        {
            addArrow = true;
            var root = ViewUtils.AddChild(GetRoot(), "arrow");
            ViewUtils.AddText(
                root,
                new Vector2(lineWidth * 10, lineWidth * 10), (int)(lineWidth*6),
                textColor: new Color(1, 1, 1, 1)).text = "▲";
        }
    }
    private bool addArrow = false;

    public FlowChartLink link
    {
        get
        {
            return _link;
        }
    }
}
