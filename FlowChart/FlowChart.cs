﻿using System.Collections.Generic;
using UnityEngine;
using UnityMVC;

public class FlowChart : Model
{
    public event EventHandler OnClicked;
    public event EventHandler OnSecondaryClicked;

    public event EventHandler OnListUpdated;
    public event EventHandler OnLinkUpdated;

    public delegate void LinkEventHandler(FlowChartLink link);
    public event LinkEventHandler OnLinkClicked;
    public event LinkEventHandler OnLinkSecondaryClicked;

    public float width = 400;
    public float height = 400;
    public float frameWidth = 400;
    public float frameHeight = 400;
    public float elementWidth = 40;
    public float elementHeight = 40;
    public float lineWidth = 6;
    public Color frameColor = new Color(0,0,0,1);

    public void AddNode(FlowChartNode newNode)
    {
        if (newNode.id == -1) newNode.id = avilableId;
        _nodes.Add(newNode);
        if (OnListUpdated != null) OnListUpdated();
    }

    public void RemoveNode(int id)
    {
        var flip = new List<FlowChartNode>();
        foreach (var node in nodes) if (node.id != id) flip.Add(node);
        _nodes = flip;

        UpdateLink();

        if (OnListUpdated != null) OnListUpdated();
    }


    public void AddLink(int fromId, int toId)
    {
        var from = GetNode(fromId);
        var to = GetNode(toId);

        if (!from.nextNodes.Exists(element => { return element == toId; })) from.nextNodes.Add(toId);
        if (!to.prevNodes.Exists(element => { return element == fromId; })) to.prevNodes.Add(fromId);

        UpdateLink();
    }

    public void RemoveLink(int fromId, int toId)
    {
        var from = GetNode(fromId);
        var to = GetNode(toId);

        if (from.nextNodes.Exists(element => { return element == toId; })) from.nextNodes.Remove(toId);
        if (to.prevNodes.Exists(element => { return element == fromId; })) to.prevNodes.Remove(fromId);

        UpdateLink();
    }

    public int avilableId
    {
        get
        {
            for(var i = 1; i < int.MaxValue; i++)
            {
                if (_nodes.Exists(element => { return element.id == i; })) continue;
                return i;
            }
            return 0;
        }
    }


    public void Click()
    {
        if (OnClicked != null) OnClicked();
    }
    public void SecondaryClick()
    {
        if (OnSecondaryClicked != null) OnSecondaryClicked();
    }

    public void ClickLink(FlowChartLink link)
    {
        if (OnLinkClicked != null) OnLinkClicked(link);
    }
    public void SecondaryClickLink(FlowChartLink link)
    {
        if (OnLinkSecondaryClicked != null) OnLinkSecondaryClicked(link);
    }


    private void UpdateLink()
    {
        foreach(var node in nodes)
        {
            var lostToIds = new List<int>();
            foreach (var toId in node.nextNodes)
            {
                if (!
                    nodes.Exists(element => {
                        return element.id == toId;
                    }))
                {
                    lostToIds.Add(toId);
                }
            }
            var lostFromIds = new List<int>();
            foreach (var fromId in node.nextNodes)
            {
                if (!
                    nodes.Exists(element => {
                        return element.id == fromId;
                    }))
                {
                    lostFromIds.Add(fromId);
                }
            }

            foreach (var lostToId in lostToIds)
            {
                node.nextNodes.Remove(lostToId);
            }
            foreach (var lostFromId in lostFromIds)
            {
                node.prevNodes.Remove(lostFromId);
            }
        }

        if (OnLinkUpdated != null) OnLinkUpdated();
    }



    public FlowChartNode GetNode(int id)
    {
        return _nodes.Find(element =>
        {
            return element.id == id;
        });
    }

    private List<FlowChartNode> _nodes = new List<FlowChartNode>();
    public List<FlowChartNode> nodes
    {
        get
        {
            return _nodes;
        }
    }

    public List<FlowChartLink> links
    {
        get
        {
            var result = new List<FlowChartLink>();
            foreach(var node in nodes)
            {
                foreach (var nextId in node.nextNodes)
                {
                    result.Add(new FlowChartLink { 
                        from = node.id,
                        to = nextId
                    });
                }
            }
            return result;
        }
    }
}

public class FlowChartNode : Model
{
    public event EventHandler OnClicked;
    public event EventHandler OnSecondaryClicked;

    public float width = 40;
    public float height = 40;
    public Color frameColor = default;

    public int id = -1;
    public List<int> nextNodes = new List<int>();
    public List<int> prevNodes = new List<int>();

    public void Click()
    {
        if (OnClicked != null) OnClicked();
    }
    public void SecondaryClick()
    {
        if (OnSecondaryClicked != null) OnSecondaryClicked();
    }

    public FlowChartNode Clone()
    {
        return new FlowChartNode { };
    }
}

public class FlowChartLink
{
    public int from;
    public int to;
}
