﻿using UnityEngine;
using UnityMVC;

public class NumberVariableBarView : View
{
    public static NumberVariableBarView Attach(GameObject parent)
    {
        var root = new GameObject("NumberVariableBar");
        var view = root.AddComponent<NumberVariableBarView>();
        view.SetParent(parent);
        return view;
    }

    private int _min = 0;
    private int _max = 0;
    private IntegerDataEditor _editor = null;
    private float _width = 0;
    private float _height = 0;

    private Transform _knob = null;
    private float _knobMinX = 0;
    private float _knobMaxX = 0;

    public NumberVariableBarView SetUp(
        IntegerDataEditor editor,
        int min,
        int max,
        float width,
        float height)
    {
        _editor = editor;
        _min = min;
        _max = max;
        _width = width;
        _height = height;

        return this;
    }

    private void Start()
    {
        AddFrame(_width, _height);
        AddKnob(Mathf.FloorToInt(_height * 0.7f));
        ApplyValue(_editor.raw);
    }

    private void ApplyValue(int value)
    {
        if (_knob == null) return;

        var ratio = 1.0f * (value - _min) / (_max - _min);

        var x = _knobMinX + (_knobMaxX - _knobMinX) * ratio;
        var center = (_knobMinX + _knobMaxX) / 2;
        var diff = x - center;

        _knob.localPosition = new Vector3(
            diff,
            0,0);
    }

    private void ApplyPosition(Vector3 position)
    {
        if (_knob == null) return;

        var x = position.x;
        var center = (_knobMinX + _knobMaxX) / 2;
        var diff = x - center;

        _knob.localPosition = new Vector3(
            diff,
            0, 0);

        var ratio = _knobMaxX != _knobMinX
            ? (x - _knobMinX) / (_knobMaxX - _knobMinX)
            : 1.0f;

        var result = _min + Mathf.FloorToInt((_max - _min) * ratio);
        _editor.SetValue(result);
    }

    private void AddFrame(
        float width,
        float height)
    {
        var frame = new GameObject("frame");
        frame.transform.parent = GetRoot().transform;
        frame.transform.localPosition = Vector3.zero;
        frame.transform.localRotation = Quaternion.identity;
        frame.transform.localScale = Vector3.one;

        var image = frame.AddComponent<UnityEngine.UI.Image>();
        image.sprite = Sprite.Create(null, new Rect(0, 0, width, height), Vector2.one / 2);
        image.color = new Color(0.15f, 0.15f, 0.15f, 0.3f);

        var rectTransform = frame.GetComponent<UnityEngine.RectTransform>();
        if (rectTransform == null)
        {
            rectTransform = frame.AddComponent<UnityEngine.RectTransform>();
        }
        rectTransform.sizeDelta = new Vector2(width, height);

        var canvasPosition = GetCanvasPosition(Camera.main.WorldToScreenPoint(frame.transform.position));

        float minX = _knobMinX = canvasPosition.x + width * -0.5f;
        float maxX = _knobMaxX = canvasPosition.x + width * 0.5f;
        float minY = canvasPosition.y + height * -0.5f;
        float maxY = canvasPosition.y + height * 0.5f;
        Vector3 current = Vector3.zero;
        bool isMoveing = false; 
        var controller = frame.AddComponent<Controller>();
        controller.OnTouchBegan += position => {
            if (isMoveing) return;
            current = position;
            isMoveing = true;
        };
        controller.OnTouchMoved += (diff, duration) => {
            var position = GetCanvasPosition(current += diff);

            if (position.x < minX
            || position.x > maxX
            || position.y < minY
            || position.y > maxY)
            {
                isMoveing = false;
            }

            if (position.x < minX)
            {
                position.x = minX;
            }
            if (position.x > maxX)
            {
                position.x = maxX;
            }

            ApplyPosition(position);
        };
        controller.OnTouchFinished += (position, duration, distance) => {
            if (!isMoveing) return;

            var convertedPosition = GetCanvasPosition(position);
            if (convertedPosition.x < minX)
            {
                convertedPosition.x = minX;
            }
            if (convertedPosition.x > maxX)
            {
                convertedPosition.x = maxX;
            }

            isMoveing = false;
            ApplyPosition(convertedPosition);
        };

        var collider = frame.AddComponent<BoxCollider2D>();
        collider.size = new Vector2(width, height);
    }

    private void AddKnob(
        float knobImageSize)
    {
        var knob = new GameObject("knob");
        knob.transform.parent = GetRoot().transform;
        knob.transform.localPosition = Vector3.zero;
        knob.transform.localRotation = Quaternion.Euler(0,0,45);
        knob.transform.localScale = Vector3.one;

        var image = knob.AddComponent<UnityEngine.UI.Image>();
        image.sprite = Sprite.Create(null, new Rect(0, 0, knobImageSize, knobImageSize), Vector2.one / 2);
        image.color = new Color(1, 1, 1, 1);

        var rectTransform = knob.GetComponent<UnityEngine.RectTransform>();
        if (rectTransform == null)
        {
            rectTransform = knob.AddComponent<UnityEngine.RectTransform>();
        }
        rectTransform.sizeDelta = new Vector2(knobImageSize, knobImageSize);

        _knob = knob.transform;
    }

    private Vector3 GetCanvasPosition(Vector3 screenPosition)
    {
        var canvas = GetRoot().GetComponentInParent<Canvas>();
        var canvasScale = 1.0f / canvas.gameObject.transform.localScale.x;
        var camera = canvas.worldCamera;

        return camera.ScreenToWorldPoint(screenPosition) * canvasScale;
    }
}

