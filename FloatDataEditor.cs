﻿public class FloatDataEditor : ElementDataEditor
{
    public float raw;

    public void SetValue(float newValue)
    {
        if (newValue == raw) return;

        raw = newValue;
        UpdateValue();
    }
}

