﻿using System.Collections.Generic;

public class ChoiceDataEditor : ElementDataEditor
{
    public List<int> raw;
    public List<string> selectables;

    public int maxCount = -1;
    public bool isSingle = false;

    public void SetValue(
        List<int> newValue)
    {
        raw = newValue;
        UpdateValue();
    }
}

