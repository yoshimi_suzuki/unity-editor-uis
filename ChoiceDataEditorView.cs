﻿using System.Collections.Generic;

using UnityEngine;
using UnityMVC;

public class ChoiceDataEditorView : View
{
    public static ChoiceDataEditorView Attach(GameObject parent)
    {
        var root = new GameObject("ChoiceDataEditor");
        var view = root.AddComponent<ChoiceDataEditorView>();
        view.SetParent(parent);
        return view;
    }

    private GameObject _container = null;
    private ScrollRectView _scrollRect = null;

    private UnityEngine.UI.Image _frame = null;
    private static Color _defaultFrameColor = new Color(0.15f, 0.15f, 0.15f, 0.3f);

    private ChoiceDataEditor _editor = null;
    private Scroller _scroller = null;
    private List<ChoiceDataElementView> _views = new List<ChoiceDataElementView>();

    private Position _originOffset = new Position();

    private float _width = 30;
    private float _height = 30;
    private float _elementWidth = 30;
    private float _elementHeight = 30;
    private int _fontSize = 30;
    private int _horizontalCount = 1;

    public ChoiceDataEditorView SetUp(
        ChoiceDataEditor editor,
        float width,
        float height,
        float elementWidth = 0,
        float elementHeight = 0,
        int horizontalCount = 1,
        Position position = null,
        int fontSize = 0,
        bool forceText = false,
        Position originOffset = null)
    {
        SetModel(editor);
        _elementWidth = elementWidth;
        _elementHeight = elementHeight;
        _fontSize = fontSize == 0 ? Mathf.FloorToInt(elementHeight * 0.70f) : fontSize;
        _horizontalCount = horizontalCount;
        _originOffset = originOffset != null ? originOffset : new Position();

        SetSize(_width = width, _height = height);

        var elements = new List<ChoiceDataElement>();

        System.Func<List<int>, bool> IsSelected = indice => {

            return false;
        };

        var index = 0;
        foreach(var selectable in editor.selectables)
        {
            var targetIndex = index++;
            var element = new ChoiceDataElement {
                raw = selectable,
                width = elementWidth,
                height = elementHeight,
                forceText = forceText
            };

            element.OnSeleted += () => {

                if (_editor.raw.Contains(targetIndex))
                {
                    if (!_editor.isSingle)
                    _editor.raw.Remove(targetIndex);

                }
                else
                {
                    if (editor.maxCount == 1
                    && _editor.raw.Count >= editor.maxCount)
                    {
                        _editor.raw.Clear();
                    }
                    else if (_editor.isSingle)
                    {
                        _editor.raw.Clear();
                    }
                    else if (editor.maxCount > 1
                    && _editor.raw.Count >= editor.maxCount)
                    {
                        return;
                    }

                    _editor.raw.Add(targetIndex);
                }
                _editor.SetValue(_editor.raw);
                UpdateList(elements, _scroller.currentOffset);
            };

            elements.Add(element);
        }

        var offset = new Position();
        if (editor.raw.Count > 0)
        {
            var minIndex = int.MaxValue;
            var currentIndex = 0;
            foreach (var raw in editor.raw)
            {
                if (minIndex > raw) minIndex = raw;
                currentIndex++;
            }
            float MarginY = -1 * (_elementHeight * 1.05f > 10 ? _elementHeight * 1.05f : 10);
            offset.y = -1 * MarginY * minIndex;
        }

        SetList(elements, offset);

        if (position != null)
        {
            editor.position = position;
        }
        UpdatePosition();

        return this;
    }

    public ChoiceDataEditorView SetModel(ChoiceDataEditor editor)
    {
        base.SetModel<ChoiceDataEditor>(_editor = editor);

        editor.OnValueUpdated += () => {

        };

        return this;
    }

    public ChoiceDataEditorView DisableScroll()
    {
        _scrollRect.gameObject.SetActive(false);
        return this;
    }

    public ChoiceDataEditorView SetList(
        List<ChoiceDataElement> elements,
        Position offset)
    {

        float width = _width - 20;
        float height = _height - 20;

        var container = _container = new GameObject("container");
        container.transform.parent = GetRoot().transform;
        container.transform.localPosition = -1 * Vector3.forward;
        container.transform.localRotation = Quaternion.identity;
        container.transform.localScale = Vector3.one;

        var containerRectTransform = container.AddComponent<RectTransform>();
        containerRectTransform.sizeDelta = new Vector2(width, height);
        var maskImage = container.AddComponent<UnityEngine.UI.Image>();
        var mask = container.AddComponent<UnityEngine.UI.Mask>();
        mask.showMaskGraphic = false;

        var scrollRect = new GameObject("scroll");
        scrollRect.transform.parent = GetRoot().transform;
        scrollRect.transform.localPosition = Vector3.zero;
        scrollRect.transform.localRotation = Quaternion.identity;
        scrollRect.transform.localScale = Vector3.one;

        var controller = scrollRect.AddComponent<Controller>();
        var collider = scrollRect.AddComponent<BoxCollider2D>();
        collider.size = new Vector2(width, height);

        _scrollRect = scrollRect.AddComponent<ScrollRectView>()
            .AssignController(controller);

        UpdateList(elements, offset);

        return this;
    }


    public ChoiceDataEditorView UpdateList(
    List<ChoiceDataElement> elements,
    Position offset)
    {
        var scroller = _scroller = new Scroller();
        scroller.SetFriction(1, 2);
        _scrollRect.SetScroller(scroller);

        float MarginX = 1 * (_elementWidth * 1.05f > 10 ? _elementWidth * 1.05f : 10);
        float MarginY = -1 * (_elementHeight * 1.05f > 10 ? _elementHeight * 1.05f : 10);
        float originX = _horizontalCount > 1 ? -1 * (_width / 2 - MarginX) : 0;
        float originY = _height / 2 + MarginY;
        int index = 0;

        foreach (var view in _views) view.Detach();
        _views.Clear();

        foreach (var element in elements)
        {
            if (isDetached) return this;

            element.position = _originOffset + new Position(
                originX + (index%_horizontalCount) * MarginX,
                originY + Mathf.FloorToInt(index/ _horizontalCount) * MarginY
                );
            var view = ChoiceDataElementView.Attach(_container).SetUp(element, _fontSize);
            if (_editor.raw.Contains(index))
            {
                view.Check();
            }
            else
            {
                view.UnCheck();
            }
            Link(view);
            scroller.AddModel(element);
            _views.Add(view);

            ++index;
        }

        var scrollMax = (Mathf.CeilToInt((float)index / _horizontalCount) + 1) * -1 * MarginY - _height;
        scroller.SetLimit(0, scrollMax, 0, 0);
        scroller.SetOffset(offset.x, offset.y);

        UpdateActivity();

        return this;
    }


    private Position _oldOffest = new Position();
    private void Update()
    {
        if (_scroller == null) return;

        if (_scroller.currentOffset.x != _oldOffest.x
            || _scroller.currentOffset.y != _oldOffest.y)
        {
            UpdateActivity();
        }
        _oldOffest = _scroller.currentOffset;
    }
    private void UpdateActivity()
    {
        foreach (var view in _views)
        {
            bool inHorizontalRange = view.transform.localPosition.x >= -1 * _width / 2
                && view.transform.localPosition.x <= 1 * _width / 2;
            bool inVerticalRange = view.transform.localPosition.y >= -1 * _height / 2
                && view.transform.localPosition.y <= 1 * _height / 2;
            if (inHorizontalRange && inVerticalRange)
            {
                view.Activate();
            }
            else
            {
                view.Deactivate();
            }
        }
    }

    public ChoiceDataEditorView SetSize(
        float width,
        float height,
        int fontSize = -1)
    {
        SetUpFrame(width, height);
        return this;
    }

    public ChoiceDataEditorView SetFrameColor(
    Color color)
    {
        _frame.color = color;
        return this;
    }

    private void SetUpFrame(float width, float height)
    {
        var frameRoot = new GameObject("frame");
        frameRoot.transform.parent = GetRoot().transform;
        frameRoot.transform.localPosition = Vector3.forward;
        frameRoot.transform.localRotation = Quaternion.identity;
        frameRoot.transform.localScale = Vector3.one;
        var frameImage = _frame = frameRoot.AddComponent<UnityEngine.UI.Image>();
        frameImage.sprite = Sprite.Create(null, new Rect(0, 0, width, height), Vector2.one / 2);
        frameImage.color = _defaultFrameColor;

        var rectTransform = frameRoot.GetComponent<UnityEngine.RectTransform>();
        if (rectTransform == null)
        {
            rectTransform = frameRoot.AddComponent<UnityEngine.RectTransform>();
        }
        rectTransform.sizeDelta = new Vector2(width, height);
    }
}

